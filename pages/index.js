import { useEffect, useState } from "react";
import { MongoClient } from "mongodb";
import MeetupList from "../components/meetups/MeetupList";




function Homepage(props) {
    return <MeetupList meetups={props.meetup_list} />;
}
// export async function getServerSideProps(context) {
//     const req = context.req;
//     const res = context.res;
//     // fetc api
//     return {
//         props: {
//             meetup_list: MEETUPS
//         }
//     };
// }
export async function getStaticProps() {
    const client = await MongoClient.connect("mongodb+srv://user1:Uj84zxuvOc8iN7Ko@cluster0.n7las.mongodb.net/mymeetups?retryWrites=true&w=majority");
    const db = client.db();
    const meetupsCollection = db.collection("mymeetups");
    const MEETUPS = await meetupsCollection.find().toArray();
    client.close();


    return {
        props: {
            meetup_list: MEETUPS.map((meetup) => ({
                title: meetup.title,
                address: meetup.address,
                image: meetup.image,
                id: meetup._id.toString(),
            }))
        }
    };
}
export default Homepage;