import MeetUpDetails from "../../components/meetups/MeetUpDetails";

function MeetUpDetail(props) {
    return (
        <MeetUpDetails

            image="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Chestnut-tailed_starling_-_%E0%A6%95%E0%A6%BE%E0%A6%A0_%E0%A6%B6%E0%A6%BE%E0%A6%B2%E0%A6%BF%E0%A6%95.jpg/1024px-Chestnut-tailed_starling_-_%E0%A6%95%E0%A6%BE%E0%A6%A0_%E0%A6%B6%E0%A6%BE%E0%A6%B2%E0%A6%BF%E0%A6%95.jpg"
            title="This is Bird"
            address="some where street"
            description="Beautiful Birds are always awswomes"
        />
    )
}
export async function getStaticPaths() {
    return {
        fallback: false,
        paths: [
            {
                params: {
                    meetupId: "m1",
                },
            },
            {
                params: {
                    meetupId: "m2",
                },
            }
        ]
    };
}

export async function getStaticProps(context) {
    const meetupId = context.params.meetupId;
    console.log(meetupId)
    return {
        props: {
            meetUpData: {
                id: meetupId,
                image: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Chestnut-tailed_starling_-_%E0%A6%95%E0%A6%BE%E0%A6%A0_%E0%A6%B6%E0%A6%BE%E0%A6%B2%E0%A6%BF%E0%A6%95.jpg/1024px-Chestnut-tailed_starling_-_%E0%A6%95%E0%A6%BE%E0%A6%A0_%E0%A6%B6%E0%A6%BE%E0%A6%B2%E0%A6%BF%E0%A6%95.jpg",
                title: "This is Bird",
                address: "some where street",
                description: "Beautiful Birds are always awswomes",
            }
        }
    }
}
export default MeetUpDetail;