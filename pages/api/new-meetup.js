// api/new-meetup
import { MongoClient } from "mongodb";
async function handler(req, res) {
    if (req.method === "POST") {
        const data = req.body;
        const client = await MongoClient.connect('mongodb+srv://user1:Uj84zxuvOc8iN7Ko@cluster0.n7las.mongodb.net/mymeetups?retryWrites=true&w=majority');
        const db = client.db();
        const meetupsCollection = db.collection('mymeetups');
        const result = await meetupsCollection.insertOne(data);
        console.log(result);
        client.close();
        res.status(201).json({ message: "Data inserted !" });
    }
}
export default handler;